#!/bin/sh

set -e

if test "$EUID" -ne 0
then
    printf "sudo %s %s\n" "$0" "$*" >&2
    exit 2
fi

qubes-dom0-update
qubesctl --show-output --skip-dom0 --templates state.sls uptodate
