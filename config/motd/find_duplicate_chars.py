#!/usr/bin/env python3

import collections
import re
import sys

data = sys.stdin.read()
sys.stdin.close()
data = re.sub('\033\\[[^m]*m', '', data)
#data = '\n'.join(line[:18] for line in data.split('\n'))
counter = collections.Counter(re.sub(r'\s', '', data))
sys.stderr.write('counter={!r}\n'.format(counter))

for c in data:
    if counter[c] > 1:
        c = '\033[7m{}\033[0m'.format(c)
    sys.stdout.write(c)
sys.stdout.close()
