include:
  - dehydrated
  - systemd-reload
# - prometheus.nginx

nginx:
  pkg.installed:
    - pkgs:
      - nginx
      - apache2-utils

  service.running:
    - require:
      - pkg: nginx
    - enable: true
    - reload: true

# file.comment does not work for some reason
/etc/nginx/nginx.conf:
  file.replace:
    - pattern: '^\s*ssl_.*'
    - repl: ''
#   - backup: false
    - require:
      - pkg: nginx
    - watch_in:
      - service: nginx

/etc/nginx/sites-enabled/default:
  file.managed:
    - source: salt://config/nginx/sites/default
    - watch_in:
      - service: nginx
    - require:
      - file: /var/lib/dehydrated/acme-challenges

/etc/nginx/dhparam2048.pem:
  cmd.run:
    - name: openssl dhparam -out /etc/nginx/dhparam2048.pem 2048
    - creates: /etc/nginx/dhparam2048.pem

/etc/nginx/conf.d/ssl.conf:
  file.managed:
    - source: salt://config/nginx/conf.d/ssl.conf
    - watch_in:
      - service: nginx
    - require:
      - file: /etc/nginx/nginx.conf
      - cmd: /etc/nginx/dhparam2048.pem

/etc/systemd/system/dehydrated.service.d/reload-nginx.conf:
  file.managed:
    - source: salt://config/systemd/dehydrated.service.d/reload-nginx.conf
    - watch_in:
      - cmd: systemctl daemon-reload

/srv/www:
  file.directory:
    - user: root
    - group: root
    - dir_mode: 755
    - file_mode: 644
    - makedirs: true

#nginx-conf.d:
#  file.recurse:
#    - name: /etc/nginx/conf.d
#    - source: salt://config/nginx/conf.d/
#    - dir_mode: 0755
#    - file_mode: 0644
#    - watch_in:
#      - service: nginx
#
#html-index:
#  file.managed:
#    - name: /var/www/html/index.html
#    - source: salt://html/index-{{ grains['id'] }}.html
#    - template: jinja
#    - user: root
#    - group: root
#    - mode: 0644

# vim: ts=2 sts=2 sw=2 et
