vim:
  pkg.installed:
    - pkgs:
{% if grains.os_family != 'RedHat' %}
      - vim
#     - vim-scripts
      # vim plugins
      - vim-editorconfig
#     - vim-ctrlp
{% else %}
      - vim-enhanced
{% endif %}

  file.managed:
    - name: /etc{% if grains.os_family != 'RedHat' %}/vim{% endif %}/vimrc.local
    - source: salt://config/vim/vimrc
    - user: root
    - group: root
    - mode: 0644

{% if grains.os_family != 'RedHat' %}
vim-addon-manager:
  pkg.installed: []
  cmd.run:
    - name: "vim-addons -qw | sed -ne 's:\\tremoved$::p' | xargs vim-addons install -w"
    - onlyif: "vim-addons -qw | grep -q 'removed$'"
{% endif %}

{% if grains.os_family == 'RedHat' %}
vimrc-source-local:
  file.append:
    - name: /etc/vimrc
    - text:
      - source /etc/vimrc.local
{% endif %}

#vimpack:
#  file.recurse:
#    - name: /usr/share/vim/vimfiles/pack
#    - source: salt://config/vim/pack
#    - user: root
#    - group: root
#    - dir_mode: 0755
#    - file_mode: 0644

# vim: ft=yaml ts=2 sts=2 sw=2 et
