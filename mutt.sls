mutt:
  pkg.installed: []

/etc/Muttrc.d/gpg.rc:
  file.managed:
    - contents: ''
    - require:
      - pkg: mutt

/etc/Muttrc.d/splitgpg.rc:
  file.managed:
    - source: salt://config/mutt/splitgpg.rc
    - user: root
    - group: root
    - mode: 0644
    - require:
      - pkg: mutt

/etc/Muttrc.d/woju.rc:
  file.managed:
    - source: salt://config/mutt/woju.rc
    - user: root
    - group: root
    - mode: 0644
    - require:
      - pkg: mutt
      - file: /etc/mailcap.dvm

/etc/mailcap.dvm:
  file.managed:
    - source: salt://config/mailcap.dvm
    - template: jinja
    - user: root
    - group: root
    - mode: 0644

/usr/bin/mutt-mailboxes:
  file.managed:
    - source: salt://bin/mutt-mailboxes
    - user: root
    - group: root
    - mode: 0755

# vim: ft=yaml ts=2 sts=2 sw=2 et
