zsh:
  pkg.installed: []

zsh-vendor-functions:
  file.recurse:
    - name: /usr/share/zsh/{% if grains.os_family == 'RedHat' %}site{% else %}vendor{% endif %}-functions
    - source: salt://config/zsh/vendor-functions
    - dir_mode: 0755
    - file_mode: 0644
    - require:
      - pkg: zsh

zsh-config:
  file.recurse:
    - name: /etc{% if grains.os_family != 'RedHat' %}/zsh{% endif %}
    - source: salt://config/zsh/zdotdir
    - dir_mode: 0755
    - file_mode: 0644
    - require:
      - file: zsh-vendor-functions

# vim: ft=sls ts=2 sts=2 sw=2 et
