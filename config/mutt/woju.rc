set folder = ~/.maildir
set spoolfile = $folder
set record = +
set postponed = +/postponed
set move = no
set header_cache = ~/.cache/mutt
set editor = "vim -c 'se ft=mail tw=78' '+/^$'"
set pager_stop = yes
set sort = threads
set sort_aux = last-date-received

set rfc2047_parameters = yes

set use_from = yes
set use_envelope_from = yes
unset envelope_from_address

set header = yes
set edit_headers = yes
set autoedit = yes
set postpone = no
set recall = no
set confirmappend = no

set mime_forward = yes
set weed = yes
set hide_missing = no
set pager_index_lines = 10

set index_format = "%4C %Z %[%d.%m.%Y] %-30.30F (%?l?%4l&%4c?) %s"

set mailcap_path = /etc/mailcap.dvm


source mutt-mailboxes|

color status        brightwhite     blue
mono  status        reverse
color indicator     black           yellow
mono  indicator     reverse
color message       brightred       default
color error         brightwhite     red
mono  error         reverse

color tree          brightmagenta   default
color index         brightblue      default "~F"
color index         brightcyan      default "~O !~D"
color index         brightcyan      default "~N !~D"
color index         brightblack     default "~D"
color index         brightyellow    default "~T"
mono  index         bold                    "~N | ~O"

color normal        default         default
color bold          brightdefault   default
mono  bold          bold
color underline     brightdefault   default
mono  underline     underline
color tilde         blue            default
mono  tilde         reverse
color markers       red             default

color signature     brightblue      default
color attachment    green           default
color quoted        brightcyan      default
color quoted1       cyan            default
color quoted2       brightblue      default
color quoted3       blue            default
color search        white           yellow
mono  search        underline

color hdrdefault    cyan            default
color header        brightyellow    default "^(From|Subject):"
color header        brightcyan      default ^To:
color header        brightcyan      default ^Cc:
mono  header        bold                    "^(From|Subject|To|Cc):"

color body          brightmagenta   default "\\* [^<]+ <[^>]+> \\[[^]]+\\]:"
color body          brightmagenta   default "(^|[^[:alnum:]])on [a-z0-9 ,-]+( at [a-z0-9:,. +-]+)? wrote:"

color body          brightyellow    default "^gpg: Good signature .*"
color body          white           default "^gpg: "
color body          brightwhite     red     "^gpg: BAD signature from.*"
mono  body          bold                    "^gpg: Good signature"
mono  body          bold                    "^gpg: BAD signature from.*"

color body          brightyellow    default \
    "([a-z][a-z0-9+-]*://(((([a-z0-9_.!~*'();:&=+$,-]|%[0-9a-f][0-9a-f])*@)?((([a-z0-9]([a-z0-9-]*[a-z0-9])?)\\.)*([a-z]([a-z0-9-]*[a-z0-9])?)\\.?|[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)(:[0-9]+)?)|([a-z0-9_.!~*'()$,;:@&=+-]|%[0-9a-f][0-9a-f])+)(/([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*(;([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*)*(/([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*(;([a-z0-9_.!~*'():@&=+$,-]|%[0-9a-f][0-9a-f])*)*)*)?(\\?([a-z0-9_.!~*'();/?:@&=+$,-]|%[0-9a-f][0-9a-f])*)?(#([a-z0-9_.!~*'();/?:@&=+$,-]|%[0-9a-f][0-9a-f])*)?|(www|ftp)\\.(([a-z0-9]([a-z0-9-]*[a-z0-9])?)\\.)*([a-z]([a-z0-9-]*[a-z0-9])?)\\.?(:[0-9]+)?(/([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*(;([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*)*(/([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*(;([-a-z0-9_.!~*'():@&=+$,]|%[0-9a-f][0-9a-f])*)*)*)?(\\?([-a-z0-9_.!~*'();/?:@&=+$,]|%[0-9a-f][0-9a-f])*)?(#([-a-z0-9_.!~*'();/?:@&=+$,]|%[0-9a-f][0-9a-f])*)?)[^].,:;!)? \t\r\n<>\"]"
color body          brightyellow    default \
    "((@(([0-9a-z-]+\\.)*[0-9a-z-]+\\.?|#[0-9]+|\\[[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\]),)*@(([0-9a-z-]+\\.)*[0-9a-z-]+\\.?|#[0-9]+|\\[[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\.[0-9]?[0-9]?[0-9]\\]):)?[0-9a-z_.+%$-]+@(([0-9a-z-]+\\.)*[0-9a-z-]+\\.?|#[0-9]+|\\[[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]\\.[0-2]?[0-9]?[0-9]\\])"


# vim: ft=muttrc ts=4 sts=4 sw=4 et
