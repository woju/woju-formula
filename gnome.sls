include:
  - fonts.iosevka

gnome-gtk-3.0:
  file.recurse:
    - name: /etc/gtk-3.0
    - source: salt://config/gtk-3.0
    - user: root
    - group: root
    - dir_mode: 0755
    - file_mode: 0644

gnome-dconf:
  pkg.installed:
    - pkgs:
      - dconf{% if grains.os_family != 'RedHat' %}-cli{% endif %}

  file.recurse:
    - name: /etc/dconf/db/local.d/
    - source: salt://config/dconf
    - user: root
    - group: root
    - dir_mode: 0755
    - file_mode: 0644

  cmd.run:
    - name: dconf update
    - watch:
      - file: gnome-dconf
    - require:
      - pkg: gnome-dconf

# vim: ft=yaml ts=2 sts=2 sw=2 et
