fetchmail:
  pkg.installed: []
  file.managed:
    - name: /lib/systemd/system/fetchmail@.service
    - source: salt://config/systemd/fetchmail@.service
    - user: root
    - group: root
    - mode: 0644

# vim: ft=yaml ts=2 sts=2 sw=2 et
