/etc/systemd/logind.conf:
  file.append:
    - text:
        - HandleLidSwitch=ignore
        - HandleLidSwitchExternalPower=ignore
        - HandleLidSwitchDocked=ignore
