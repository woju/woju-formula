wireguard: pkg.installed

{% for wg in salt['pillar.get']('wireguard', ()) %}
/etc/wireguard/{{ wg }}.conf:
  file.managed:
    - source: salt://config/wireguard.conf
    - template: jinja
    - context:
        wg: {{ wg }}
    - user: root
    - group: root
    - mode: 0600
    - watch_in:
      - service: wg-quick@{{ wg }}
    - require:
      - pkg: wireguard

wg-quick@{{ wg }}:
  service.running:
    - enable: True
    - require:
      - file: /etc/wireguard/{{ wg }}.conf
{% endfor %}

# vim: ts=2 sts=2 sw=2 et
