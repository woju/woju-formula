# https://linuxmint-user-guide.readthedocs.io/en/latest/snap.html

snapd: pkg.purged

/etc/apt/preferences.d/nosnap.pref:
  file.managed:
    - source: salt://config/apt/preferences.d/nosnap.pref
    - require:
      - pkg: snapd
