/etc/motd:
  file.managed:
    - source: salt://config/motd/{{ grains.id }}

# vim: ft=yaml ts=2 sts=2 sw=2 et
