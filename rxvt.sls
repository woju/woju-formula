include:
  - fonts.iosevka

rxvt:
  pkg.installed:
    - pkgs:
      - rxvt-unicode-256color{% if grains.os_family == 'RedHat' %}-ml{% endif %}

{% if grains.os_family == 'RedHat' %}
  file.append:
    - name: /etc/X11/Xresources
    - source: salt://config/X11/Xresources/rxvt
    - template: jinja
{% else %}
  file.managed:
    - name: /etc/X11/Xresources/rxvt
    - source: salt://config/X11/Xresources/rxvt
    - template: jinja
    - user: root
    - group: root
    - mode: 0644
{% endif %}

#X11-xinitrcd:
#  file.recurse:
#    - name: /etc/X11/xinit/xinitrc.d
#    - source: salt://config/X11/xinitrc.d
#    - user: root
#    - group: root
#    - file_mode: 0755

# vim: ft=yaml ts=2 sts=2 sw=2 et
