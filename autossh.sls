autossh:
  pkg.installed: []
  user.present:
    - system: true
    - home: /etc/autossh
    - createhome: false

/etc/autossh:
  file.directory:
    - user: root
    - group: autossh
    - mode: 0750
    - require:
      - user: autossh

/lib/systemd/system/autossh@.service:
  file.managed:
    - source: salt://config/systemd/autossh@.service
    - user: root
    - group: root
    - mode: 0644

{% for remote, data in salt['pillar.get']('autossh', {}).items() %}
{% set default_path = '/etc/default/autossh-' + remote %}
{% set sshkey_path = '/etc/autossh/' + remote %}

ssh_known_hosts-{{ remote }}:
  file.append:
    - name: /etc/ssh/ssh_known_hosts
    - text:
      {% for line in data.known_hosts %}
        - {{ line }}
      {% endfor %}

{{ sshkey_path }}:
  file.managed:
    - user: autossh
    - group: autossh
    - mode: 0400
    - contents_pillar: autossh:{{ remote }}:sshkey
    - require:
      - user: autossh

{{ default_path }}:
  file.managed:
    - user: root
    - group: root
    - contents: |
        SSH_OPTIONS="-v -N -D {{ data.port }} -l {{ data.user }} -i {{ sshkey_path }} {{ remote }}"
    - require:
      - file: {{ sshkey_path }}

autossh@{{ remote }}:
  service.running:
    - enable: true
    - require:
      - file: /lib/systemd/system/autossh@.service
      - file: {{ default_path }}
    - watch:
      - file: {{ default_path }}

{% endfor %}
