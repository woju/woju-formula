include:
  - nginx
  - systemd-reload

dehydrated:
  pkg.installed: []
  user.present:
    - shell: /usr/sbin/nologin
    - home: /var/lib/dehydrated
    - system: true
  alias.present:
    - target: root
  cmd.run:
    - name: dehydrated -c
    - runas: dehydrated
    - require:
      - user: dehydrated
      - file: /var/lib/dehydrated/acme-challenges
      - file: /etc/dehydrated/domains.txt
      - service: nginx
    - watch:
      - file: /etc/dehydrated/domains.txt

/etc/systemd/system/dehydrated.service.d: file.directory

/lib/systemd/system/dehydrated.service:
  file.managed:
    - source: salt://config/systemd/dehydrated.service
    - require:
      - file: /etc/systemd/system/dehydrated.service.d

dehydrated.timer:
  file.managed:
    - name: /lib/systemd/system/dehydrated.timer
    - source: salt://config/systemd/dehydrated.timer
    - require:
      - file: /lib/systemd/system/dehydrated.service
    - watch_in:
      - cmd: systemctl daemon-reload
  service.running:
    - enable: true
    - require:
      - file: dehydrated.timer

/var/lib/dehydrated/acme-challenges:
  file.directory:
    - user: dehydrated
    - group: dehydrated
    - require:
      - user: dehydrated

/var/lib/dehydrated:
  file.directory:
    - user: dehydrated
    - require:
      - user: dehydrated

/etc/dehydrated/domains.txt:
  file.managed:
    - source: salt://config/dehydrated/domains-{{ grains.id }}.txt
    - template: jinja


/etc/cron.d/dehydrated: file.absent
# file.managed:
#   - contents: |
#       25 1 * * 0 root su -s /bin/sh -c "dehydrated -c" dehydrated >/dev/null && systemctl reload nginx
#   - contents: |
#       25 1 * * 0 root su -s /bin/sh -c "dehydrated -c" dehydrated >/dev/null && /etc/haproxy/mkcerts.sh && systemctl reload haproxy


# vim: ts=2 sts=2 sw=2 et
