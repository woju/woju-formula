git:
  pkg.installed:
    - pkgs:
      - git
{% if grains.os_family != 'RedHat' %}
      - libterm-readkey-perl
{% else %}
      - perl-TermReadKey
{% endif %}

/etc/gitconfig:
  file.managed:
    - source: salt://config/git/gitconfig
    - mode: 0644
    - require:
      - pkg: git
      - file: /etc/gitignore
      - file: /etc/gitattributes

/etc/gitignore:
  file.managed:
    - source: salt://config/git/gitignore
    - mode: 0644

/etc/gitattributes:
  file.managed:
    - source: salt://config/git/gitattributes
    - mode: 0644
