packages-installed-openembedded:
  pkg.installed:
  - pkgs:
      - kas
      - chrpath
      - diffstat
      # TODO kas-container
      - docker.io
