fonts-iosevka:
  archive.extracted:
    - name: /usr/share/fonts/iosevka
    - source: salt://distfiles/super-ttc-iosevka-10.3.0.zip
    - enforce_toplevel: false

# vim: ft=yaml ts=2 sts=2 sw=2 et
