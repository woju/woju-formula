tmux:
  pkg.installed: []
  file.managed:
    - name: /etc/tmux.conf
    - source: salt://config/tmux.conf
    - user: root
    - group: root
    - mode: 0644

# vim: ft=yaml ts=2 sts=2 sw=2 et
