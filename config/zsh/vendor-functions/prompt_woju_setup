#!/bin/zsh

# psvar:
#  1 - high nice
#  2 - low nice

prompt_woju_setup() {
    autoload -Uz vcs_info add-zsh-hook

    setopt prompt_subst

    zstyle ':vcs_info:*'	              actionformats       '%F{b}[%%b%F{blue}%s%B%F{black}|%F{red}%c%u%F{black}|%F{blue}%b%F{blue}%F{black}|%F{red}%a%F{black}]%f '
    zstyle ':vcs_info:*'	              formats		          '%F{b}[%%b%F{blue}%s%B%F{black}|%F{red}%c%u%F{black}|%F{blue}%b%F{blue}%F{black}]%f '
    zstyle ':vcs_info:*'	              check-for-changes   true
    zstyle ':vcs_info:(sv[nk]|bzr):*'   branchformat        '%b%F{1}:%F{3}%r'

    # FIXME somewhere offsets are calculated wrong
    PS1=\
"%(?..%F{red}%? )%B%(1j.%F{black}[%F{magenta}%j job%(2j.s.)%F{black}] .)"\
"%F{black}[%b%F{yellow}`uname -m` %B%y%F{black}] "\
"%(1V.[%F{green}%1v%F{black}] .)%(2V.[%F{red}%2v%F{black}] .)"\
"%(!.%F{red}.%F{green}%n@)%m %F{blue}%~ \${vcs_info_msg_0_}"\
"%F{black}[%F{cyan}%T %h%F{black}]%F{white}%# %f%b"
    PS2='`%_> '
    PS3='?# '
    PS4='+%N:%i:%_> '
    SPROMPT="zsh: correct '%F{red}%B%R%f%b' to '%F{green}%B%r%f%b' %B[nyae]%b? "

#   EXITCODE="%(?..%?%1v )"
#   SPROMPT="zsh: correct '${fg_bold[red]}%R${reset_color}' to '${fg_bold[green]}%r${reset_color}' "$'\E'"[${color[bold]}m[nyae]${reset_color}? "

    add-zsh-hook precmd prompt_woju_precmd
    add-zsh-hook preexec prompt_woju_preexec
}

prompt_woju_set_title() {
    local title="zsh[%~]"
    if [[ $# -gt 0 ]]; then
        title="${title}: $*"
    fi

    if [[ "${TERM}" = screen* ]]
    then
        print -Pn "\ek${title}\e\134"
    elif [[ ${terminfo[hs]} = yes ]]
    then
        print -Pn "${terminfo[tsl]}${title}${terminfo[fsl]}"
    fi
}

prompt_woju_precmd() {
    vcs_info

    unset psvar
    local nice=$(nice)
    if [[ $nice -gt 0 ]]; then
        psvar[1]=$nice
    elif [[ $nice -lt 0 ]]; then
        psvar[2]=$nice
    fi

    prompt_woju_set_title
}

prompt_woju_preexec() {
    prompt_woju_set_title "$1"
}

prompt_woju_setup "$@"

# vim: ts=4 sw=4 et
