# woju-formula

![Jesus, that's a lot of yaml files](yaml-files.png)

A bunch of my configz, collected from different places. Use at your own peril.

## HOWTO

### new service which uses X.509 certs from dehydrated
- drop reloader in `/etc/systemd/system/dehydrated.service.d`

### setup in qubes
- import key to dom0 (gittrust.kbx)
- checkout to `/srv/formulas/base/woju-formula`
    - `/etc/salt/minion.d/woju-formula.conf`, possibly copied from other formula
- setup tops (`/srv/salt/woju.top`, `qubesctl top.enable woju.top`)
- put something for each template (TODO what?, to not skip it during uptodate)

## NOTES TO SELF:
- `pkg.latest` does not work in dom0 (dunno why)
