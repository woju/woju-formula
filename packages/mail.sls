include:
  - mutt
  - fetchmail

packages-installed-mail:
  pkg.installed:
  - pkgs:
      - postfix
      - procmail
      - rsync  # for /rw/config/rc.local

# vim: ft=yaml ts=2 sts=2 sw=2 et
