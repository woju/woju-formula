gdb: pkg.installed

/etc/gdb/gdbinit:
  file.managed:
    - source: salt://config/gdbinit
    - mode: 0644
    - require:
      - pkg: gdb
