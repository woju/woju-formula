fonts-terminus:
  pkg.installed:
    - pkgs:
{% if grains.os_family != 'RedHat' %}
      - xfonts-terminus
{% else %}
      - terminus-fonts
{% endif %}

# vim: ft=yaml ts=2 sts=2 sw=2 et
